import * as express from "express";
import { Request, Response } from "express";

const router = express.Router();

/* GET home page. */
router.get('/', function(req: Request, res: Response, next: Function) {
    res.render('index', { title: 'Welcome to course route' })
});

module.exports = router;

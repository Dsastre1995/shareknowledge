"use strict";
var Sequelize = require("sequelize");
var seq = new Sequelize('shareKnowledge', 'root', 'dsastre1234', { dialect: 'mysql' });
exports.UserModel = seq.define('user', {
    firstName: { type: String, required: true },
    lastName: { type: String, required: false },
    email: { type: String, required: true },
    password: { type: String, required: true },
    createdAt: { type: Date, required: false },
    updatedAt: { type: Date, required: false }
});

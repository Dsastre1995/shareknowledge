import * as Sequelize from "sequelize";
const seq = new Sequelize('shareKnowledge', 'root', 'dsastre1234', {dialect: 'mysql'});

export const ImageModel = seq.define('image', {
    url: { type: String, required: true },
    tag: { type: String, required: true }
});
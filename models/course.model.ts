import * as Sequelize from "sequelize";
import { UserModel } from "./user.model";
import { ImageModel } from "./image.model";
const seq = new Sequelize('shareKnowledge', 'root', 'dsastre1234', {dialect: 'mysql'});

export const CourseModel = seq.define('course', {
    title: { type: String, required: true },
    description: { type: String, required: true },
    url: { type: String, required: true },
    userEmail: { type: String, required: true },
    userPassword: { type: String, required: true }
});

CourseModel.belongsTo(ImageModel);
CourseModel.belongsTo(UserModel);

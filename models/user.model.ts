import * as Sequelize from "sequelize";
const seq = new Sequelize('shareKnowledge', 'root', 'dsastre1234', {dialect: 'mysql'});

export const UserModel = seq.define('user', {
    firstName: { type: String, required: true },
    lastName: { type: String, required: false },
    email: { type: String, required: true },
    password: { type: String, required: true },
    createdAt: { type: Date, required: false },
    updatedAt: { type: Date, required: false }
});
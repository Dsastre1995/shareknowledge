"use strict";
var Sequelize = require("sequelize");
var user_model_1 = require("./user.model");
var image_model_1 = require("./image.model");
var seq = new Sequelize('shareKnowledge', 'root', 'dsastre1234', { dialect: 'mysql' });
exports.CourseModel = seq.define('course', {
    title: { type: String, required: true },
    description: { type: String, required: true },
    url: { type: String, required: true },
    userEmail: { type: String, required: true },
    userPassword: { type: String, required: true }
});
exports.CourseModel.belongsTo(image_model_1.ImageModel);
exports.CourseModel.belongsTo(user_model_1.UserModel);
